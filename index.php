<!DOCTYPE html>
<html lang="test">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SIMANS ELEVATORS</title>
<meta name="description" content="SIMANS Elevators is promoted by people with vast experience in this field . We are Pleased to inform that Simans Elevators is one of the best-organized team of lift professionals. Simans offering the lifts with latest features and options at economical and affordable price range.">
<meta name="author" content="">
<meta name="KEYWORDS" content="Service, &, Customer, Care, International, Technical, Service Simans, Elevators, Escalators, Moving, Walks, Installation, Service, Modernization,Lift Service,Elevator Maintenance Services,Lift Service in kochi,Kochi,Cochin,Lift Modernization,Car Lift,Bed Lift,Simans Elevators,Lift Maintenance,Dump Waiter Lift,Services, Hospital Lift, Stretcher Lift, Goods Lift">


<link rel="icon" href="images/favicon.ico">

<!-- Web Fonts
================================================== -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800,800i%7CPlayfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
<link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">
<link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/plugins.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/colors.css" />

<!-- IE
================================================== -->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


</head>

<body>
<div id="boxed-layout"> 
  
  <!-- Top Bar
================================================== -->
  <div id="topbar">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-8">
          <ul class="topbar-contact">
            <li><i class="fa fa-phone-square"></i>(+91) 9207159887, (+91) 9207519887</li>
            <li>|</li>
            <li class="language dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i>simanselevators245@gmail.com
 
          </ul>
          <!-- end .topbar-contact --> 
        </div>
        <!-- end .col-md-5 col-sm-5 -->
        
        <div class="col-md-4 col-sm-4">
          <ul class="social-icons pull-right">
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://www.facebook.com/simanselevators/"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-flickr"></i></a></li>
            <li><a href="#"><i class="fa fa-rss"></i></a></li>
          </ul>
          <!-- end .social-icons --> 
        </div>
        <!-- end .col-md-7 col-sm-7 --> 
        
      </div>
      <!-- end .row --> 
    </div>
    <!-- end .container --> 
  </div>
  <!-- end #topbar --> 
  
  <!-- Header
================================================== -->
  <header id="header">
    <div class="header-shadow"> </div>
    <!-- end .header-shadow -->
    <nav class="navbar yamm navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand" href="index.php">
          <img src="images/logo.png" alt="" />
          </a> </div>
        <!-- end .navbar-header -->
        
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li><a href="about.php">About Us</a></li>
            <li><a href="service.php">Service</a></li>
            <li><a href="products.php">Products</a></li>
            <li><a href="gallery">Gallery</a></li>
            <li><a href="contactus.php">Contact Us</a></li>
          <ul>  
          <!-- end .nav navbar-nav --> 
        </div>
        <!-- end .navbar-collapse collapse --> 
        
      </div>
      <!-- end .container --> 
    </nav>
    <!-- end .navbar --> 
    
  </header>
  <!-- end #header -->
  <div class="clearfix"> </div>
  <!-- end .clearfix --> 
  
  <!-- Slider
================================================== -->
  <div id="rev-slider1-wrapper" class="rev_slider_wrapper fullscreen-container">
    <div id="rev-slider1" class="rev_slider fullscreenbanner" data-version="5.1.6">
      <ul>
        
        <!-- SLIDE 1  -->
        <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1000"  data-thumb="#" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Thumb Title 1" data-description=""> 
          
          <!-- MAIN IMAGE -->
          <img src="images/slider/slider600-1.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
          
          <!-- LAYER NR. 0 -->
          <div class="tp-caption tp-resizeme rs-parallaxlevel-0" 
		id="slide-1-layer-0" 
		data-x="right" data-hoffset="100" 
		data-y="bottom" data-voffset="0" 
		data-width="none"
		data-height="none"
		data-whitespace="nowrap"
		data-transform_idle="o:1;"
		data-transform_in="y:10px;opacity:0;s:2000;e:Power3.easeInOut;" 
		data-transform_out="y:10px;opacity:0;s:1500;e:Power2.easeIn;" 
		data-start="1500" 
		data-end="8000" 
		data-basealign="grid" 
		data-responsive_offset="on" 
		data-responsive="on"
		style="z-index: 6;">
            <img src="#" alt="">
          </div>
          
          <!-- LAYER NR. 1 -->
          <div class="tp-caption white-large black-bg tp-resizeme rs-parallaxlevel-0"
		id="slide-1-layer-1"
		data-x="left" data-hoffset="['10','10','10','10']"
		data-y="middle" data-voffset="['-80','-70','-50','-40']"
		data-fontsize="['50','50','40','25']"
		data-lineheight="['80','80','64','40']"
		data-width="none"
		data-height="none"
		data-whitespace="nowrap"
		data-transform_idle="o:1;"
		data-transform_in="y:-100px;opacity:0;s:2000;e:Power3.easeInOut;"
		data-transform_out="y:-100px;opacity:0;s:1500;e:Power2.easeIn;"
		data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
		data-mask_out="x:0px;y:0px;s:inherit;e:inherit;"
		data-start="1500"
		data-end="8000"
		data-splitin="none"
		data-splitout="none"
		data-basealign="grid"
		data-responsive_offset="off"
		data-responsive="off"
		style="z-index:10;">Welcome Simans Elevators PVT LTD</div>
          <!-- end .tp-caption --> 
          
          <!-- LAYER NR. 2 -->
          <div class="tp-caption white-small black-bg tp-resizeme rs-parallaxlevel-0"
		id="slide-1-layer-2"
		data-x="left" data-hoffset="['10','10','10','10']"
		data-y="middle" data-voffset="['0','0','0','0']"
		data-fontsize="['25','25','18','13']"
		data-lineheight="['40','40','30','20']"
		data-width="none"
		data-height="none"
		data-whitespace="nowrap"
		data-transform_idle="o:1;"
		data-transform_in="y:-100px;opacity:0;s:2000;e:Power3.easeInOut;"
		data-transform_out="y:-100px;opacity:0;s:1500;e:Power2.easeIn;"
		data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
		data-mask_out="x:0px;y:0px;s:inherit;e:inherit;"
		data-start="2000"
		data-end="8000"
		data-splitin="none"
		data-splitout="none"
		data-basealign="grid"
		data-responsive_offset="off"
		data-responsive="off"
		style="z-index:10;">Simans Elevators is one of the best-organized team of lift professionals</div>
          <!-- end .tp-caption --> 
          
         
        </li>
        
        <!-- SLIDE 2  -->
        <li data-index="rs-2" data-transition="fade" data-slotamount="default" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1000"  data-thumb="images/slider/thumb/thumb2.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Thumb Title 2" data-description=""> 
          
          <!-- MAIN IMAGE -->
          <img src="images/slider/slider600-2.jpg"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-no-retina>
          
          <!-- LAYER NR. 0 -->
          
          <!-- LAYER NR. 1 -->
          <div class="tp-caption white-large black-bg tp-resizeme rs-parallaxlevel-0"
		id="slide-2-layer-1"
		data-x="right" data-hoffset="['10','10','10','10']"
		data-y="middle" data-voffset="['-80','-70','-50','-40']"
		data-fontsize="['50','50','40','25']"
		data-lineheight="['80','80','64','40']"
		data-width="none"
		data-height="none"
		data-whitespace="nowrap"
		data-transform_idle="o:1;"
		data-transform_in="y:-100px;opacity:0;s:2000;e:Power3.easeInOut;"
		data-transform_out="y:-100px;opacity:0;s:1500;e:Power2.easeIn;"
		data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
		data-mask_out="x:0px;y:0px;s:inherit;e:inherit;"
		data-start="1500"
		data-end="8000"
		data-splitin="none"
		data-splitout="none"
		data-basealign="grid"
		data-responsive_offset="off"
		data-responsive="off"
		style="z-index:10; text-align:right;">Dedicated Teams</div>
          <!-- end .tp-caption --> 
          
          <!-- LAYER NR. 2 -->
          <div class="tp-caption white-small black-bg tp-resizeme rs-parallaxlevel-0"
		id="slide-2-layer-2"
		data-x="right" data-hoffset="['10','10','10','10']"
		data-y="middle" data-voffset="['0','0','0','0']"
		data-fontsize="['25','25','18','13']"
		data-lineheight="['40','40','30','20']"
		data-width="none"
		data-height="none"
		data-whitespace="nowrap"
		data-transform_idle="o:1;"
		data-transform_in="y:-100px;opacity:0;s:2000;e:Power3.easeInOut;"
		data-transform_out="y:-100px;opacity:0;s:1500;e:Power2.easeIn;"
		data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
		data-mask_out="x:0px;y:0px;s:inherit;e:inherit;"
		data-start="2000"
		data-end="8000"
		data-splitin="none"
		data-splitout="none"
		data-basealign="grid"
		data-responsive_offset="off"
		data-responsive="off"
		style="z-index:10; text-align:right;">Simans Elevators Team is vibrant energetic group having ten year experience in this field .</div>
          <!-- end .tp-caption --> 
         
        </li>
        
        <!-- SLIDE 3  -->
        <li data-index="rs-3" data-transition="fade" data-slotamount="default" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1000"  data-thumb="images/slider/thumb/thumb3.jpg" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="Thumb Title 1" data-description=""> 
          
          <!-- MAIN IMAGE -->
          <img src="images/slider/slider600-3.jpg" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
          
          <!-- LAYER NR. 1 -->
          <div class="tp-caption white-large black-bg tp-resizeme rs-parallaxlevel-0"
		id="slide-3-layer-1"
		data-x="left" data-hoffset="['10','10','10','10']"
		data-y="middle" data-voffset="['-80','-70','-50','-40']"
		data-fontsize="['50','50','40','25']"
		data-lineheight="['80','80','64','40']"
		data-width="none"
		data-height="none"
		data-whitespace="nowrap"
		data-transform_idle="o:1;"
		data-transform_in="y:-100px;opacity:0;s:2000;e:Power3.easeInOut;"
		data-transform_out="y:-100px;opacity:0;s:1500;e:Power2.easeIn;"
		data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
		data-mask_out="x:0px;y:0px;s:inherit;e:inherit;"
		data-start="1500"
		data-end="8000"
		data-splitin="none"
		data-splitout="none"
		data-basealign="grid"
		data-responsive_offset="off"
		data-responsive="off"
		style="z-index:10;">Uncompromising maintenance</div>
          <!-- end .tp-caption --> 
          
          <!-- LAYER NR. 2 -->
          <div class="tp-caption white-small black-bg tp-resizeme rs-parallaxlevel-0"
		id="slide-3-layer-2"
		data-x="left" data-hoffset="['10','10','10','10']"
		data-y="middle" data-voffset="['0','0','0','0']"
		data-fontsize="['25','25','18','13']"
		data-lineheight="['40','40','30','20']"
		data-width="none"
		data-height="none"
		data-whitespace="nowrap"
		data-transform_idle="o:1;"
		data-transform_in="y:-100px;opacity:0;s:2000;e:Power3.easeInOut;"
		data-transform_out="y:-100px;opacity:0;s:1500;e:Power2.easeIn;"
		data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
		data-mask_out="x:0px;y:0px;s:inherit;e:inherit;"
		data-start="2000"
		data-end="8000"
		data-splitin="none"
		data-splitout="none"
		data-basealign="grid"
		data-responsive_offset="off"
		data-responsive="off"
		style="z-index:10;">Don't take our word for it, read what our recent clients are saying about our service.</div>
          <!-- end .tp-caption --> 
        
        </li>
      </ul>
      <div class="tp-static-layers"> </div>
      <!-- end .tp-static-layers -->
      <div class="tp-bannertimer" style="visibility: hidden !important;"> </div>
      <!-- end .tp-bannertimer --> 
    </div>
    <!-- end #rev-slider1 --> 
  </div>
  <!-- end #rev-slider1-wrapper -->
  <div class="mb-100 clearfix"> </div>
  <!-- end .mb-100 clearfix --> 
  
  <!-- Icon Box 1
================================================== -->
  <div class="container">
    <div class="row animated delay5" data-animation="fadeInUp">
      <div class="col-md-3 col-sm-3">
        <h3>Welcome to <span class="text-theme">Simans</span></h3>
        <div class="titleline2"> </div>
        <!-- end .titleline2 -->
        
        <p>SIMANS  Elevators is promoted by people with vast experience in  this field . We are Pleased to inform that Simans Elevators is one of the best-organized team of lift professionals. Simans  offering the lifts with latest features and options at economical and affordable price range.</p>
      </div>
      <!-- end .col-md-3 col-sm-3 -->
      
      <div class="col-md-3 col-sm-3">
        <div class="iconbox1 frame3">
          <div class="iconbox1-icon"> <a href="about.php"><i class="fa fa-users effect-2"></i></a> </div>
          <!-- end .iconbox1-icon -->
          <h4>Professional Team</h4>
          <div class="iconbox1-bg"><i class="fa fa-building-o"></i></div>
          <p>Our team is vibrant energetic  <br>group having ten year experience in this field <p>
          <p><a href="about.php" class="btn btn-theme">Read More<span><i class="fa fa-chevron-right"></i></span></a></p>
        </div>
        <!-- end .iconbox1 --> 
      </div>
      <!-- end .col-md-3 col-sm-3 -->
      
      <div class="col-md-3 col-sm-3">
        <div class="iconbox1 frame3">
          <div class="iconbox1-icon"> <a href="about.php"><i class="fa fa-trophy effect-2"></i></a> </div>
          <!-- end .iconbox1-icon -->
          <h4>Our Mission</h4>
          <div class="iconbox1-bg"><i class="fa fa-trophy"></i></div>
          <p>To offer our customers a great  <br>technical and professional team collaborates.</p>
          <p><a href="about.php" class="btn btn-theme">Read More<span><i class="fa fa-chevron-right"></i></span></a></p>
        </div>
        <!-- end .iconbox1 --> 
      </div>
      <!-- end .col-md-3 col-sm-3 -->
      
      <div class="col-md-3 col-sm-3">
        <div class="iconbox1 frame3">
          <div class="iconbox1-icon"> <a href="#"><i class="fa fa-smile-o effect-2"></i></a> </div>
          <!-- end .iconbox1-icon -->
          <h4>Customer Satisfation</h4>
          <div class="iconbox1-bg"><i class="fa fa-smile-o"></i></div>
          <p>Key objective of SIMANS is  <br>total customer satisfaction & provide safe lift operation  </p>
          <p><a href="about.php" class="btn btn-theme">Read More<span><i class="fa fa-chevron-right"></i></span></a></p>
        </div>
        <!-- end .iconbox1 --> 
      </div>
      <!-- end .col-md-3 col-sm-3 --> 
      
    </div>
    <!-- end .row --> 
  </div>
  <!-- end .container -->
  
  <!-- end .mb-100 --> 
  
  <!-- Five Images Carousel Wide
================================================== -->
  <div class="wrapper-slashbg">
    <div class="container">
      <div class="row animated delay5" data-animation="fadeInUp">
        <div class="col-md-12 col-sm-12 text-center">
          <h3>Oru <span class="text-theme">Products</span></h3>
          <div class="titleline2-center"> </div>
          <!-- end .titleline2-center -->
          <p class="lead">We offer a wide range of Lifts in a variety of sizes and specifications. Our creative team are also able to fully design and produce lifting equipment that adhere to your individual needs, however unique they may be. Our products include a range of passenger and goods lifts that are both cost effective and reliable for use in residential properties, offices, hospitals, hotels, industrial and shopping centres. We are also providers of industrial doors, cranes, hoists, lifting tackle and dock levellers.</p>
        </div>
        <!-- end .col-md-12 col-sm-12 --> 
        
      </div>
      <!-- end .row --> 
    </div>
    <!-- end .container -->
    
    <div class="owl-image owl-carousel owl-theme black-arrow">
      <div class="item">
        <div class="da-thumbs">
          <img src="images/products/thumb1.jpg" alt=""/>
          <div> <span>  <a  class="nivo-lightbox" href="images/products/1.jpg" title="Passenger Lift" data-lightbox-gallery="gallery1"><i class="fa fa-search effect-2"></i></a> </span> </div>
        </div>
        <!-- end .da-thumbs -->
        <div class="clearfix"> </div>
        <div class="photo-title text-left2">
          <h5>Passenger Lift</h5>
          <p>We provide a wide range of high quality, low maintenance passenger lifts that can be used for a variety of applications, from residential, offices and hospitals, to hotels, shopping centres and more.</p>
          <p><a href="products.php">Read More <i class="fa fa-angle-double-right"></i></a></p>
        </div>
        <!-- end .photo-title --> 
      </div>
      <!-- end .item -->
      
      <div class="item">
        <div class="da-thumbs">
          <img src="images/products/thumb2.jpg" alt=""/>
          <div> <span>  <a  class="nivo-lightbox" href="images/products/2.jpg" title="Service Lift" data-lightbox-gallery="gallery1"><i class="fa fa-search effect-2"></i></a> </span> </div>
        </div>
        <!-- end .da-thumbs -->
        <div class="clearfix"> </div>
        <div class="photo-title text-left2">
          <h5>Service Lift</h5>
          <p>We provide a wide range of high quality, low maintenance Service lifts that can be used for a variety of applications, from residential, offices and hospitals, to hotels, shopping centres and more.</p>
          <p><a href="products.php">Read More <i class="fa fa-angle-double-right"></i></a></p>
        </div>
        <!-- end .photo-title --> 
      </div>
      <!-- end .item -->
      
      <div class="item">
        <div class="da-thumbs">
          <img src="images/products/thumb3.jpg" alt=""/>
          <div> <span> <a  class="nivo-lightbox" href="images/products/3.jpg" title="Dumb Waiter Lift" data-lightbox-gallery="gallery1"><i class="fa fa-search effect-2"></i></a> </span> </div>
        </div>
        <!-- end .da-thumbs -->
        <div class="clearfix"> </div>
        <div class="photo-title text-left2">
          <h5>Dumb Waiter</h5>
          <p>We hold a wide range of dumbwaiters here at SIMANS ELEVATORS (P) Ltd and will not only design and build your dumbwaiter for you, but our friendly staff will also install it in..</p>
          <p><a href="products.php">Read More <i class="fa fa-angle-double-right"></i></a></p>
        </div>
        <!-- end .photo-title --> 
      </div>
      <!-- end .item -->
      
      <div class="item">
        <div class="da-thumbs">
          <img src="images/products/thumb4.jpg" alt=""/>
          <div> <span><a  class="nivo-lightbox" href="images/products/4.jpg" title="This is an image title" data-lightbox-gallery="gallery1"><i class="fa fa-search effect-2"></i></a> </span> </div>
        </div>
        <!-- end .da-thumbs -->
        <div class="clearfix"> </div>
        <div class="photo-title text-left2">
          <h5>Car Lift</h5>
          <p>We offers all types of vehicle/car elevators ranging from 1500 kg cars to 5000 kg cars. Lately, Car Elevators are an essential feature in commercial and residential buildings..</p>
          <p><a href="products.php">Read More <i class="fa fa-angle-double-right"></i></a></p>
        </div>
        <!-- end .photo-title --> 
      </div>
      <!-- end .item -->
      
      <div class="item">
        <div class="da-thumbs">
          <img src="images/products/thumb5.jpg" alt=""/>
          <div> <span> <a  class="nivo-lightbox" href="images/products/5.jpg" title="This is an image title" data-lightbox-gallery="gallery1"><i class="fa fa-search effect-2"></i></a> </span> </div>
        </div>
        <!-- end .da-thumbs -->
        <div class="clearfix"> </div>
        <div class="photo-title text-left2">
          <h5>Bed Lift</h5>
          <p>Bed Lifts or Hospital Elevators are mainly used for efficient, smooth and swift vertical transportation of patients on wheel-chairs or stretchers with medical equipments, doctors, nurses and other attendants</p>
          <p><a href="products.php">Read More <i class="fa fa-angle-double-right"></i></a></p>
        </div>
        <!-- end .photo-title --> 
      </div>
      <!-- end .item -->
      
   
      
    </div>
    <!-- end .owl-image --> 
  </div>
  <!-- end .wrapper-slashbg -->



  <!-- Wide Box Gray
================================================== -->
  <div class="widebox-gray">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 col-sm-6 hidden-xs"> </div>
        <!-- end .col-md-6 col-sm-6 -->
        
        <div class="col-md-6 col-sm-6 widebox-gray-content skew-gray-left">
          <h3><span class="text-theme">Our </span>Service</h3>
          <div class="titleline"> </div>
          <!-- end .titleline -->
          
          <p>We are proud to claim that we have a strong team of qualified , well experienced expert technicians who have proven be the best in this field . Our team includes qualified professionals who are extremely skilled in their area of know-how . Upholding services to the total satisfaction of our customers at an efficiently reasonable price.</p>
          <p> Our skills are continuously upgraded for the end user's in areas of product upgradation & safety. We do impart our knowledge sharing with our user’s through proper usage & basic maintenance of equipment’s with all norms  in place . We at SIMANS Elevators desire to serve customers by clearly understanding their needs.</p>
          <div class="mb-20"> </div>
          <!-- end .mb-20 -->
          <p><a href="service.php" class="btn btn-theme">Read More<span><i class="fa fa-chevron-right"></i></span></a></p>
        </div>
        <!-- end .col-md-6 col-sm-6 --> 
        
      </div>
      <!-- end .row --> 
    </div>
    <!-- end .container-fluid --> 
  </div>
  <!-- end .widebox-gray --> 
  
  <!-- Parallax Black - Testimonial
================================================== -->
  <!-- end .parallax-black --> 
  
  <!-- Process
================================================== -->
 
  <!-- end .wrapper-graybg -->
  <div class="mb-100"> </div>
  <!-- end .mb-100 --> 
  
  <!-- About Us
================================================== -->
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 text-center">
        <h3>About <span class="text-theme">Us</span></h3>
        <div class="titleline2-center"> </div>
        <!-- end .titleline2-center -->
        <p class="lead">SIMANS  Elevators is promoted by people with vast experience in  this field . We are Pleased to inform that Simans Elevators is one of the best-organized team of lift professionals. Simans  offering the lifts with latest features and options at economical and affordable price range. We are the  committed to provide highest quality services during all stages of your project from new Installation to maintenance of all makes and brands . The focus on professional service and reliability are the key attributes of this long business relationship that we would like to establish with you.</p>
      </div>
      <!-- end .col-md-12 col-sm-12 --> 
    </div>
    <!-- end .row -->
    <div class="mb-20"> </div>
    <!-- end .mb-20 -->
    
    <div class="row animated delay5" data-animation="fadeInUp">
      <div class="col-md-6 col-sm-6">
        <h4>Our Skills</h4>
        <div class="skillbar-title"> Lift Services</div>
        <!-- end .skillbar-title -->
        <div class="skillbar">
          <div class="skillbar-percent skillbar-olive skillbar-striped" data-percent="100%"> <span class="skillbar-tooltip">100%</span> </div>
        </div>
        <!-- end .skillbar -->
        <div class="skillbar-title"> Lift Repairs </div>
        <!-- end .skillbar-title -->
        <div class="skillbar">
          <div class="skillbar-percent skillbar-blue skillbar-striped" data-percent="90%"> <span class="skillbar-tooltip">90%</span> </div>
        </div>
        <!-- end .skillbar -->
        <div class="skillbar-title"> Lift Refurbishments</div>
        <!-- end .skillbar-title -->
        <div class="skillbar">
          <div class="skillbar-percent skillbar-aqua skillbar-striped" data-percent="90%"> <span class="skillbar-tooltip">90%</span> </div>
        </div>
        <!-- end .skillbar -->
        <div class="skillbar-title">Thorough Examination loler</div>
        <!-- end .skillbar-title -->
        <div class="skillbar">
          <div class="skillbar-percent skillbar-green skillbar-striped" data-percent="80%"> <span class="skillbar-tooltip">80%</span> </div>
        </div>
        <!-- end .skillbar --> 
      </div>
      <!-- end .col-md-4 col-sm-4 -->
       
      <div class="col-md-4 col-sm-4">
        <h4>Why Choose Us</h4>
        <ul class="icon-arrow-list">
          <li><i class="fa fa-check-square"></i><a href="#">We are Established.</a></li>
          <li><i class="fa fa-check-square"></i><a href="#">We are Innovative.</a></li>
          <li><i class="fa fa-check-square"></i><a href="#">We are Safe And Secure.</a></li>
          <li><i class="fa fa-check-square"></i><a href="#">We are Highly Efficient.</a></li>
          <li><i class="fa fa-check-square"></i><a href="#">We are Cost Effective.</a></li>
          <li><i class="fa fa-check-square"></i><a href="#">We are Worth The Money.</a></li>

          </ul>
        
      </div>

      <!-- end .col-md-4 col-sm-4 -->
      
      
      
    </div>
    <!-- end .row --> 
  </div>
  <!-- end .container -->
  <div class="mb-100"> </div>
  <!-- end .mb-100 --> 
  
  <!-- Footer
================================================== -->
 <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="footer-about"> <a href="index.php">
            <img src="images/logo-white.png" alt="" />
            </a>
            <p>SIMANS  Elevators is promoted by people with vast experience in this field . We are Pleased to inform that Simans Elevators is one of the best-organized team of lift professionals.</p>
          </div>
          <!-- end .footer-about --> 
        </div>
        <!-- end .col-md-3 col-sm-3 -->
        
        <div class="col-md-3 col-sm-3">
          <h4>Quick Links</h4>
          <div class="titleline2-footer"> </div>
          <!-- end .titleline2-footer -->
          <ul class="footer-list">
            <li><i class="fa fa-angle-right"></i><a href="service.php">Our Service</a></li>
            <li><i class="fa fa-angle-right"></i><a href="about.php">Who we are?</a></li>
            <li><i class="fa fa-angle-right"></i><a href="products.php">Our Products</a></li>
            <li><i class="fa fa-angle-right"></i><a href="gallery.php">Galllery</a></li>
            <li><i class="fa fa-angle-right"></i><a href="contactus.php">Conatct Us</a></li>
          </ul>
        </div>
        <!-- end .col-md-3 col-sm-3 -->
        
       
        <!-- end .col-md-3 col-sm-3 -->
        
        <div class="col-md-3 col-sm-3">
          
          <!-- end .footer-tooltip -->
          <h4>Contact Us</h4>
          <div class="titleline2-footer"> </div>
          <!-- end .titleline2-footer -->
          <ul class="footer-list">
            <li><i class="fa fa-map-marker"></i><span>Address:</span><br> Room no.67/9082(Second Floor), Panickassery building, Convent Road , Ernakulam,Pin : 682035</li>
            <li><i class="fa fa-phone"></i><span>Phone number(Office):</span><br> (0484) 2363194,(+91)9207159887,(+91)9207519887</li>
            <li><i class="fa fa-envelope"></i><span>E-mail:</span><br> <a href="mailto:simanselevators245@gmail.com
">simanselevators245@gmail.com
</a></li>
          </ul>
        </div>

        <div class="col-md-3 col-sm-3">
        <div class="footer-tooltip">Get In Touch
            <div class="footer-tooltip-arrow"></div>
          </div>
          <h4>Contact Form</h4>
          <div class="titleline2-footer"> </div>
          <!-- end .titleline2-footer -->
          <form class="commentForm3 cmxform" method="post" action="http://www.colorsthemes.com/archit/contact3.php">
            <fieldset>
              <input class="form-control" id="txtName" name="name" minlength="2" type="text" placeholder="Name *" required>
              <input class="form-control" id="txtEmail" type="email" name="email" placeholder="E-mail *" required>
              <input class="form-control" id="txtPhoneNo" type="text" name="subject" placeholder="Phone No *" required>
              <input class="form-control" id="txtSubject" type="text" name="subject" placeholder="Subject *" required>
              <textarea class="form-control" id="txtMessage" name="comment" placeholder="Message *" required></textarea>
             
              <input class="submit btn btn-theme" id="btnContact" name="submit" type="button" value="Send Message">
            </fieldset>
          </form>
          <div class="message3"> </div>
          <!-- end .message3 --> 
        </div>
        <!-- end .col-md-3 col-sm-3 --> 
        
      </div>
      <!-- end .row --> 
    </div>
    <!-- end .container --> 
  </footer>
  
  <!-- Copyright
================================================== -->
  <div id="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="copyright"> &copy; 2017 Simans. All Rights Reserved. </div>
          <!-- end .copyright --> 
        </div>
        <!-- end .col-md-6 col-sm-6 -->
        
      <!--   <div class="col-md-6 col-sm-6">
          <ul class="copyright-link">
            <li><a href="faq.html">FAQ</a></li>
            <li><a href="sitemap.html">Sitemap</a></li>
            <li><a href="contact.html">Contact</a></li>
          </ul>
        </div> -->
        <!-- end .col-md-6 col-sm-6 --> 
        
      </div>
      <!-- end .row --> 
    </div>
    <!-- end .container --> 
  </div>
  <!-- end #copyright --> 
</div>
<!-- end #boxed-layout -->

<div id="back-to-top"> <a href="#">Back to Top</a> </div>
<!-- end #back-to-top --> 

<!-- Java Script
================================================== --> 
<script src="js/jquery.min.js"></script> 
<script src="js/jquery-migrate.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script> 
<script src="js/plugins.js"></script> 
<script src="js/custom.js"></script> 
<script src="js/main.js"></script> 
<script type="text/javascript">

var tpj = jQuery;
var revapi1;
tpj(document).ready(function() {
    if (tpj("#rev-slider1").revolution == undefined) {
        revslider_showDoubleJqueryError("#rev-slider1");
    } else {
        revapi1 = tpj("#rev-slider1").show().revolution({
            sliderLayout: "fullwidth", // auto, fullwidth, fullscreen
            sliderType: "standard", // standard, hero, carousel
            delay: 9000,
            spinner: "off", // off, spinner0, spinner1, spinner2, spinner3, spinner4, spinner5
            disableProgressBar: "off", // on, off
            dottedOverlay: "none", // none,twoxtwo, threexthree, twoxtwowhite, threexthreewhite
            jsFileLocation: "revolution/js/",
            gridwidth: 1200,
            gridheight: 600,
            responsiveLevels: [1200, 992, 768, 480],
            autoHeight: "off",
            parallax: {
                type: "mouse", // off, mouse, scroll, mouse+scroll
                origo: "slidercenter", // slidercenter, enterpoint
                speed: 2000,
                levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
                disable_onmobile: "on"
            },
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "off", // on, off
                touch: {
                    touchenabled: "on",
                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    swipe_direction: "horizontal",
                    drag_block_vertical: false
                },
                arrows: {
                    style: "zeus",
                    enable: true,
                    hide_onmobile: false,
                    hide_under: 600,
                    hide_onleave: true,
                    hide_delay: 200,
                    hide_delay_mobile: 1200,
                    tmp: "",
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 30,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 30,
                        v_offset: 0
                    }
                },
                bullets: {
                    style: "zeus",
                    enable: true, // true, false
                    hide_onmobile: false,
                    hide_under: 960,
                    hide_onleave: true,
                    direction: "horizontal", // horizontal, vertical
                    h_align: "right", // left, center, right
                    v_align: "bottom", // top, center, bottom
                    h_offset: 80,
                    v_offset: 50,
                    space: 5,
                    tmp: '<span class="tp-bullet-image"></span> <span class="tp-bullet-imageoverlay"></span> <span class="tp-bullet-title">{{title}}</span>'
                }
            },
            lazyType: "none", // none, full, smart, single
            shadow: 0, // 0, 1, 2, 3, 4, 5, 6
            shuffle: "off",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false
            }
        });

        var newCall = new Object(),
            cslide;
        newCall.callback = function() {
            var proc = revapi1.revgetparallaxproc(),
                fade = 1 + proc,
                scale = 1 + (Math.abs(proc) / 10);
            punchgs.TweenLite.set(revapi1.find(".slotholder, .rs-background-video-layer"), {
                opacity: fade,
                scale: scale
            });
        }
        newCall.inmodule = "parallax";
        newCall.atposition = "start";
        revapi1.on("revolution.slide.onloaded", function(e) {
            revapi1.revaddcallback(newCall);
        });
    }
});
</script> 


<script type="text/javascript" src="revolution/js/extensions/revolution.extension.actions.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.carousel.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.kenburn.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.layeranimation.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.migration.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.navigation.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.parallax.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.slideanims.min.js"></script> 
<script type="text/javascript" src="revolution/js/extensions/revolution.extension.video.min.js"></script> 



</body>

</html>
