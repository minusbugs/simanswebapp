 <?php include('includes/header.php') ?>
 	  <!-- Page Title
================================================== -->
  <div class="pagetitle">
    <div class="pagetitle-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <h1>Gallery</h1>
          </div>
          <!-- end .col-md-6 col-sm-6 -->
          
          <div class="col-md-6 col-sm-6">
            <ul class="breadcrumb">
              <li><a href="index.html">Home</a></li>
              <li class="active">Gallery</li>
            </ul>
          </div>
          <!-- end .col-md-6 col-sm-6 --> 
          
        </div>
        <!-- end .row --> 
      </div>
      <!-- end .container --> 
    </div>
    <!-- end .pagetitle-overlay --> 
  </div>
  <!-- end .pagetitle -->
  <div class="mb-100"> </div>
  <!-- end .mb-100 --> 
  <div class="container">
 	 <div class="isotope">
 	 <?php  
 	  $log_directory = "images/portfolio/";
 	  foreach(glob($log_directory.'/*.jpg') as $file) {  ?>
   			
		<div class="col-md-3 col-sm-6 element-item filtering1">
          <div class="da-thumbs">
            <img src="<?php echo $file; ?>" alt=""/>
            <div> <span><a class="nivo-lightbox" href="<?php echo $file; ?>" title ="test" data-lightbox-gallery="gallery1"><i class="fa fa-search effect-2"></i></a> </span> </div>
          </div>
          <!-- end .da-thumbs --> 
        </div>
 	 <?php  	
		}
 	 ?>
 	 	
 	 </div>
  </div>
 <?php include('includes/footer.php') ?>