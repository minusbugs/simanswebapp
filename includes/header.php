<!DOCTYPE html>
<html lang="test">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>SIMANS ELEVATORS</title>
<meta name="description" content="SIMANS Elevators is promoted by people with vast experience in this field . We are Pleased to inform that Simans Elevators is one of the best-organized team of lift professionals. Simans offering the lifts with latest features and options at economical and affordable price range.">
<meta name="author" content="">
<meta name="KEYWORDS" content="Service, &, Customer, Care, International, Technical, Service Simans, Elevators, Escalators, Moving, Walks, Installation, Service, Modernization,Lift Service,Elevator Maintenance Services,Lift Service in kochi,Kochi,Cochin,Lift Modernization,Car Lift,Bed Lift,Simans Elevators,Lift Maintenance,Dump Waiter Lift,Services, Hospital Lift, Stretcher Lift, Goods Lift">
<link rel="icon" href="images/favicon.ico">

<!-- Web Fonts
================================================== -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800,800i%7CPlayfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="revolution/css/settings.css">
<link rel="stylesheet" type="text/css" href="revolution/css/layers.css">
<link rel="stylesheet" type="text/css" href="revolution/css/navigation.css">
<link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/plugins.css" />
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/colors.css" />

<!-- IE
================================================== -->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Style Switcher (Demo Only)
================================================== -->
</head>

<body>
<div id="boxed-layout"> 
  
  <!-- Top Bar
================================================== -->
  <div id="topbar">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-8">
          <ul class="topbar-contact">
            <li><i class="fa fa-phone-square"></i>(+91) 9207159887,(+91) 9207519887</li>
            <li>|</li>
            <li class="language dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i>simanselevators245@gmail.com
 
          </ul>
          <!-- end .topbar-contact --> 
        </div>
        <!-- end .col-md-5 col-sm-5 -->
        
        <div class="col-md-4 col-sm-4">
          <ul class="social-icons pull-right">
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://www.facebook.com/simanselevators/"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-flickr"></i></a></li>
            <li><a href="#"><i class="fa fa-rss"></i></a></li>
          </ul>
          <!-- end .social-icons --> 
        </div>
        <!-- end .col-md-7 col-sm-7 --> 
        
      </div>
      <!-- end .row --> 
    </div>
    <!-- end .container --> 
  </div>
  <!-- end #topbar --> 
  
  <!-- Header
================================================== -->
  <header id="header">
    <div class="header-shadow"> </div>
    <!-- end .header-shadow -->
    <nav class="navbar yamm navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand" href="index.php">
          <img src="images/logo.png" alt="" />
          </a> </div>
        <!-- end .navbar-header -->
        
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li><a href="about.php">About Us</a></li>
            <li><a href="service.php">Service</a></li>
            <li><a href="products.php">Products</a></li>
            <li><a href="gallery.php">Gallery</a></li>
            <li><a href="contactus.php">Contact Us</a></li>
          <ul>  
          <!-- end .nav navbar-nav --> 
        </div>
        <!-- end .navbar-collapse collapse --> 
        
      </div>
      <!-- end .container --> 
    </nav>
    <!-- end .navbar --> 
    
  </header>
  <!-- end #header -->
  <div class="clearfix"> </div>
  <!-- end .clearfix --> 