 <!-- Footer
================================================== -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="footer-about"> <a href="index.php">
            <img src="images/logo-white.png" alt="" />
            </a>
            <p>SIMANS  Elevators is promoted by people with vast experience in this field . We are Pleased to inform that Simans Elevators is one of the best-organized team of lift professionals.</p>
          </div>
          <!-- end .footer-about --> 
        </div>
        <!-- end .col-md-3 col-sm-3 -->
        
        <div class="col-md-3 col-sm-3">
          <h4>Quick Links</h4>
          <div class="titleline2-footer"> </div>
          <!-- end .titleline2-footer -->
          <ul class="footer-list">
            <li><i class="fa fa-angle-right"></i><a href="service.php">Our Service</a></li>
            <li><i class="fa fa-angle-right"></i><a href="about.php">Who we are?</a></li>
            <li><i class="fa fa-angle-right"></i><a href="products.php">Our Products</a></li>
            <li><i class="fa fa-angle-right"></i><a href="gallery.php">Galllery</a></li>
            <li><i class="fa fa-angle-right"></i><a href="contactus.php">Conatct Us</a></li>
          </ul>
        </div>
        <!-- end .col-md-3 col-sm-3 -->
        
       
        <!-- end .col-md-3 col-sm-3 -->
        
        <div class="col-md-3 col-sm-3">
          
          <!-- end .footer-tooltip -->
          <h4>Contact Us</h4>
          <div class="titleline2-footer"> </div>
          <!-- end .titleline2-footer -->
          <ul class="footer-list">
            <li><i class="fa fa-map-marker"></i><span>Address:</span><br> Room no.67/9082(Second Floor), Panickassery building, Convent Road , Ernakulam,Pin : 682035</li>
            <li><i class="fa fa-phone"></i><span>Phone number(Office):</span><br> (0484) 2363194,(+91)9207159887,(+91)9207519887</li>
            <li><i class="fa fa-envelope"></i><span>E-mail:</span><br> <a href="mailto:simanselevators245@gmail.com
">simanselevators245@gmail.com
</a></li>
          </ul>
        </div>

        <div class="col-md-3 col-sm-3">
        <div class="footer-tooltip">Get In Touch
            <div class="footer-tooltip-arrow"></div>
          </div>
          <h4>Contact Form</h4>
          <div class="titleline2-footer"> </div>
          <!-- end .titleline2-footer -->
          <form class="commentForm3 cmxform" method="post" action="#">
            <fieldset>
              <input class="form-control" id="txtName" name="name" minlength="2" type="text" placeholder="Name *" required>
              <input class="form-control" id="txtEmail" type="email" name="email" placeholder="E-mail *" required>
              <input class="form-control" id="txtPhoneNo" type="text" name="subject" placeholder="Phone No *" required>
              <input class="form-control" id="txtSubject" type="text" name="subject" placeholder="Subject *" required>
              <textarea class="form-control" id="txtMessage" name="comment" placeholder="Message *" required></textarea>
             
              <input class="submit btn btn-theme" id="btnContact" name="submit" type="button" value="Send Message">
            </fieldset>
          </form>
          <div class="message3"> </div>
          <!-- end .message3 --> 
        </div>
        <!-- end .col-md-3 col-sm-3 --> 
        
      </div>
      <!-- end .row --> 
    </div>
    <!-- end .container --> 
  </footer>
  <!-- end #footer --> 
  
  <!-- Copyright
================================================== -->
  <div id="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="copyright"> &copy; 2017 Simans. All Rights Reserved. </div>
          <!-- end .copyright --> 
        </div>
        <!-- end .col-md-6 col-sm-6 -->
        
      <!--   <div class="col-md-6 col-sm-6">
          <ul class="copyright-link">
            <li><a href="faq.html">FAQ</a></li>
            <li><a href="sitemap.html">Sitemap</a></li>
            <li><a href="contact.html">Contact</a></li>
          </ul>
        </div> -->
        <!-- end .col-md-6 col-sm-6 --> 
        
      </div>
      <!-- end .row --> 
    </div>
    <!-- end .container --> 
  </div>
  <!-- end #copyright --> 
</div>
<!-- end #boxed-layout -->

<div id="back-to-top"> <a href="#">Back to Top</a> </div>
<!-- end #back-to-top --> 

<!-- Java Script
================================================== --> 
<script src="js/jquery.min.js"></script> 
<script src="js/jquery-migrate.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script> 
<script src="js/plugins.js"></script> 
<script src="js/custom.js"></script> 
<script src="js/main.js"></script> 




</body>