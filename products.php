<?php include('includes/header.php') ?>

<div class="pagetitle">
    <div class="pagetitle-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <h1>Our Products</h1>
          </div>
          <!-- end .col-md-6 col-sm-6 -->
          
          <div class="col-md-6 col-sm-6">
            <ul class="breadcrumb">
              <li><a href="index.php">Home</a></li>
              <li class="active">Our Products</li>
            </ul>
          </div>
          <!-- end .col-md-6 col-sm-6 --> 
          
        </div>
        <!-- end .row --> 
      </div>
      <!-- end .container --> 
    </div>
    <!-- end .pagetitle-overlay --> 
  </div>
  <!-- end .pagetitle -->
  <div class="mb-100"> </div>
  <!-- end .mb-100 --> 
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <h3>Products</h3>
        <div class="titleline2"> </div>
        <!-- end .titleline2 --> 
      </div>
      <!-- end .col-md-12 col-sm-12 --> 
    </div>
    <!-- end .row --> 
  </div>
  <div class="mb-20"> </div>
  <!-- end .mb-20 -->
  
  <div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="timeline">
          <div class="da-thumbs">
            <img src="images/products/thumb1.jpg" alt=""/>
            <div> <span> <a class="nivo-lightbox" href="images/products/1.jpg" title="Passenger Elevators" data-lightbox-gallery="gallery3"><i class="fa fa-search effect-2"></i></a> </span> </div>
          </div>
          <!-- end .da-thumbs -->
          <div class="clearfix"> </div>
          <!-- end .clearfix --> 
        </div>
        <!-- end .timeline --> 
      </div>
      <!-- end .col-md-6 col-sm-6 -->
      
      <div class="col-md-6 col-sm-6">
        <div class="timeline-details">
          <h4>Passenger Elevators</h4>
          <div class="titleline2"> </div>
          <!-- end .titleline2 -->
          <p>Simans offers all types of passenger elevators with the latest technology applications. Passenger elevators are categorized according to the size, weight, speed and the number of passengers transported vertically. Generally passenger elevators are available in capacities ranging from 4 passengers to 21 passengers with speed varying between .5 m/sec and 2.5 m/sec. Choice of elevator type may vary according to the type of building (Commercial, Residential Apartments, Hotels, Hospitals, etc), number of floors, passenger traffic and the budget involved..</p>
        </div>
        <!-- end .timeline-details --> 
      </div>
      <!-- end .col-md-6 col-sm-6 --> 
    </div>
   </div>

   <div class="mb-20"> </div>
  <!-- end .mb-20 -->
  
  <div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="timeline">
          <div class="da-thumbs">
            <img src="images/products/thumb2.jpg" alt=""/>
            <div> <span> <a class="nivo-lightbox" href="images/products/2.jpg" title="Service Elevators" data-lightbox-gallery="gallery3"><i class="fa fa-search effect-2"></i></a> </span> </div>
          </div>
          <!-- end .da-thumbs -->
          <div class="clearfix"> </div>
          <!-- end .clearfix --> 
        </div>
        <!-- end .timeline --> 
      </div>
      <!-- end .col-md-6 col-sm-6 -->
      
      <div class="col-md-6 col-sm-6">
        <div class="timeline-details">
          <h4>Service Elevators</h4>
          <div class="titleline2"> </div>
          <!-- end .titleline2 -->
          <p>From 50Kgs to 10,000Kgs we can provide a lift tailored to your needs. We have a wide range of designs that are cost- effective, reliable and durable. Our goods lifts can come with their own self-supporting structure to allow for quick installation and minimal builders works as a dedicated shaft will not be required. </p>
        </div>
        <!-- end .timeline-details --> 
      </div>
      <!-- end .col-md-6 col-sm-6 --> 
    </div>
   </div>

   <div class="mb-20"> </div>

   <div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="timeline">
          <div class="da-thumbs">
            <img src="images/products/thumb3.jpg" alt=""/>
            <div> <span> <a class="nivo-lightbox" href="images/products/3.jpg" title="Dumb Waiter" data-lightbox-gallery="gallery3"><i class="fa fa-search effect-2"></i></a> </span> </div>
          </div>
          <!-- end .da-thumbs -->
          <div class="clearfix"> </div>
          <!-- end .clearfix --> 
        </div>
        <!-- end .timeline --> 
      </div>
      <!-- end .col-md-6 col-sm-6 -->
      
      <div class="col-md-6 col-sm-6">
        <div class="timeline-details">
          <h4>Dumb Waiter</h4>
          <div class="titleline2"> </div>
          <!-- end .titleline2 -->
          <p>We hold a wide range of dumbwaiters here at R. J. Lift Services Ltd and will not only design and build your dumbwaiter for you, but our friendly staff will also install it in. </p>
        </div>
        <!-- end .timeline-details --> 
      </div>
      <!-- end .col-md-6 col-sm-6 --> 
    </div>
   </div>

   <div class="mb-20"> </div>

   <div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="timeline">
          <div class="da-thumbs">
            <img src="images/products/thumb4.jpg" alt=""/>
            <div> <span> <a class="nivo-lightbox" href="images/products/4.jpg" title="Car Elevators" data-lightbox-gallery="gallery3"><i class="fa fa-search effect-2"></i></a> </span> </div>
          </div>
          <!-- end .da-thumbs -->
          <div class="clearfix"> </div>
          <!-- end .clearfix --> 
        </div>
        <!-- end .timeline --> 
      </div>
      <!-- end .col-md-6 col-sm-6 -->
      
      <div class="col-md-6 col-sm-6">
        <div class="timeline-details">
          <h4>Car Elevators</h4>
          <div class="titleline2"> </div>
          <!-- end .titleline2 -->
          <p>SIMANS ELEVATORS (P) LTD offers all types of vehicle/car elevators ranging from 1500 kg cars to 5000 kg cars. Lately, Car Elevators are an essential feature in commercial and residential buildings.</p>
        </div>
        <!-- end .timeline-details --> 
      </div>
      <!-- end .col-md-6 col-sm-6 --> 
    </div>
   </div>

   <div class="mb-20"> </div>

   <div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="timeline">
          <div class="da-thumbs">
            <img src="images/products/thumb5.jpg" alt=""/>
            <div> <span> <a class="nivo-lightbox" href="images/products/5.jpg" title="Bed Elevators" data-lightbox-gallery="gallery3"><i class="fa fa-search effect-2"></i></a> </span> </div>
          </div>
          <!-- end .da-thumbs -->
          <div class="clearfix"> </div>
          <!-- end .clearfix --> 
        </div>
        <!-- end .timeline --> 
      </div>
      <!-- end .col-md-6 col-sm-6 -->
      
      <div class="col-md-6 col-sm-6">
        <div class="timeline-details">
          <h4>Bed Elevators</h4>
          <div class="titleline2"> </div>
          <!-- end .titleline2 -->
          <p>Bed Lifts or Hospital Elevators are mainly used for efficient, smooth and swift vertical transportation of patients on wheel-chairs or stretchers with medical equipments, doctors, nurses and other attendants. Axiomata Elevators Pvt. Ltd offers the most efficient and dynamic looking bed lifts of all types and best suiting dimensions.</p>
        </div>
        <!-- end .timeline-details --> 
      </div>
      <!-- end .col-md-6 col-sm-6 --> 
    </div>
   </div>
<?php include('includes/footer.php') ?>