<?php include('includes/header.php') ?>
<!-- Page Title
================================================== -->
  <div class="pagetitle">
    <div class="pagetitle-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <h1>About</h1>
          </div>
          <!-- end .col-md-6 col-sm-6 -->
          
          <div class="col-md-6 col-sm-6">
            <ul class="breadcrumb">
              <li><a href="index.php">Home</a></li>
              <li class="active">About</li>
            </ul>
          </div>
          <!-- end .col-md-6 col-sm-6 --> 
          
        </div>
        <!-- end .row --> 
      </div>
      <!-- end .container --> 
    </div>
    <!-- end .pagetitle-overlay --> 
  </div>
  <!-- end .pagetitle -->
  <div class="mb-100"> </div>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <div class="owl-single owl-carousel owl-theme">
          <div class="item">
            <img src="images/features/feature1.jpg" alt="" />
          </div>
          
        </div>
        <!-- end .owl-single --> 
      </div>
      <!-- end .col-md-6 col-sm-6 -->
      
      <div class="col-md-6 col-sm-6">
        <h3>About Us</h3>
        <div class="titleline2"> </div>
        <!-- end .titleline2 -->
        <p>SIMANS  Elevators is promoted by people with vast experience in this field . We are Pleased to inform that Simans Elevators is one of the best-organized team of lift professionals. Simans  offering the lifts with latest features and options at economical and affordable price range. We are the  committed to provide highest quality services during all stages of your project from new Installation to maintenance of all makes and brands . The focus on professional service and reliability are the key attributes of this long business relationship that we would like to establish with you.</p>
        <div class="row">
        <h4>WHY CHOOSE US? </h4>
          <div class="col-xs-6">
            <ul class="icon-arrow-list">
              <li><i class="fa fa-check-square"></i>We are Established.</li>
               <li><i class="fa fa-check-square"></i>We are Innovative.</li>
               <li><i class="fa fa-check-square"></i>We are Safe And Secure.</li>
            </ul>
          </div>
          <!-- end .col-xs-6 -->
          <div class="col-xs-6">
            <ul class="icon-arrow-list">
              <li><i class="fa fa-check-square"></i>We are Highly Efficient.</li>
             <li><i class="fa fa-check-square"></i>We are Cost Effective.</li>
             <li><i class="fa fa-check-square"></i>We are Worth The Money.</li>
            </ul>
          </div>
          <!-- end .col-xs-6 --> 
        </div>
        <!-- end .row --> 
        
      </div>
      <!-- end .col-md-6 col-sm-6 --> 
      
    </div>
    <!-- end .row --> 

    <div class="row">
         <div class="col-md-6 col-sm-6">
          <div class="iconbox2">
            <div class="iconbox2-bg"> <i class="fa fa fa-users"></i> </div>
            <div class="iconbox2-icon"> <i class="fa fa fa-users effect-2"></i> </div>
            <div class="iconbox2-content">
              <h5>PROFESSIONAL TEAM</h5>
              <p>We Simans Elevators Team is vibrant energetic group having ten year experience in this field of new installations, modernization and maintenance of all types of Elevators . We utilizes the most advanced state-of-the-art technologies available. </p>
            </div>
          </div>
          <!-- end .iconbox2 --> 
        </div>
         <div class="col-md-6 col-sm-6">
          <div class="iconbox2">
            <div class="iconbox2-bg"> <i class="fa fa-smile-o"></i> </div>
            <div class="iconbox2-icon"> <i class="fa fa-smile-o effect-2"></i> </div>
            <div class="iconbox2-content">
              <h5>CUSTOMER SATISFACTION</h5>
              <p>Key objective of SIMANS is total customer satisfaction.  Our mission is to provide safe lift operation and customer friendly service by continuously improving the operational effectiveness without compromising quality in service.</p>
            </div>
          </div>
          <!-- end .iconbox2 --> 
        </div>
      </div>
      <!-- end .row --> 

  </div>
  <!-- end .container -->

  </div>
  <?php include('includes/footer.php') ?>