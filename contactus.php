<?php include('includes/header.php') ?>
 <!-- Page Title
================================================== -->
  <div class="pagetitle">
    <div class="pagetitle-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <h1>Contact</h1>
          </div>
          <!-- end .col-md-6 col-sm-6 -->
          
          <div class="col-md-6 col-sm-6">
            <ul class="breadcrumb">
              <li><a href="index.html">Home</a></li>
              <li class="active">Contact</li>
            </ul>
          </div>
          <!-- end .col-md-6 col-sm-6 --> 
          
        </div>
        <!-- end .row --> 
      </div>
      <!-- end .container --> 
    </div>
    <!-- end .pagetitle-overlay --> 
  </div>
  <!-- end .pagetitle -->
  <div class="mb-100"> </div>
  <!-- end .mb-100 --> 
   <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-8">
        <div class="contactform">
          <h3>CONTACT US</h3>
          <div class="titleline2"> </div>
          <!-- end .titleline2 -->
          <form class="commentForm cmxform" method="post" action="http://www.colorsthemes.com/archit/contact.php">
            <fieldset>
              <input class="form-control" id="txtName"  name="name" minlength="2" type="text" placeholder="Name *" required>
              <input class="form-control" id="txtEmail" type="email" name="email" placeholder="E-mail *" required>
               <input class="form-control" id="txtPhoneNo" type="text" name="subject" placeholder="Phone No *" required>
              <input class="form-control" id="txtSubject" type="text" name="subject" placeholder="Subject *" required>
              <textarea class="form-control"  id="txtMessage" name="comment" placeholder="Message *" required></textarea>
              <input class="submit btn btn-theme"  id="btnContact" name="submit" type="button" value="Send Message">
            </fieldset>
          </form>
          <div class="message"> </div>
          <!-- end .message --> 
        </div>
        <!-- end .contactform --> 
      </div>
      <!-- end .col-md-8 col-sm-8 --> 
      
      <!-- SIDEBAR START -->
      <div class="col-md-4 col-sm-4">
        <div class="sidebar">
          <h3>CONTACT INFORMATION</h3>
          <div class="titleline2"> </div>
          <!-- end .titleline2 -->
          
          <ul class="sidebar-info">
            <li><i class="fa fa-map-marker"></i><span>Address:</span><br> Room no.67/9082(Second Floor), Panickassery building, Convent Road , Ernakulam,Pin : 682035</li>
           <li><i class="fa fa-phone"></i><span>Phone number:</span><br> (0484) 2363194,(+91)9207159887,(+91)9207519887</li>
            <li><i class="fa fa-envelope"></i><span>E-mail:</span><br> <a href="mailto:simanselevators245@gmail.com
">simanselevators245@gmail.com
</a></li>
          </ul>
          <div class="mb-10"> </div>
          <!-- end .mb-10 -->
          
          <ul class="social-icons">
            <li><a href="https://www.facebook.com/simanselevators/"><i class="fa fa-facebook" data-toggle="tooltip" data-placement="top" title="Facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-flickr" data-toggle="tooltip" data-placement="top" title="Flickr"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter" data-toggle="tooltip" data-placement="top" title="Twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-skype" data-toggle="tooltip" data-placement="top" title="Skype"></i></a></li>
            <li><a href="#"><i class="fa fa-instagram" data-toggle="tooltip" data-placement="top" title="Instagram"></i></a></li>
          </ul>
          <div class="mb-20 clearfix"> </div>
          <!-- end .mb-20 clearfix --> 
          
        </div>
        <!-- end .sidebar --> 
        
      </div>
      <!-- end .col-md-4 col-sm-4 --> 
      <!-- SIDEBAR END --> 
      
    </div>
    <!-- end .row --> 
  </div>
  <!-- end .container -->
  <div class="mb-100"> </div>
  <!-- end .mb-100 --> 
    <div class="map map400"> </div>

<?php include('includes/footer.php') ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-23VQ_uoe-4j1EjXSUD85uJI4pm1JkrY"></script> 
<script type="text/javascript" src="js/jquery.gmap.min.js"></script> 
<script type="text/javascript">
        $('.map').gMap({
            maptype: 'ROADMAP',  // 'HYBRID', 'SATELLITE', 'ROADMAP' or 'TERRAIN'
            zoom: 15,
            markers:[
                       {
                        address: "Liberty Island, New York, NY 10004, USA",  // Your Address here
                        html: "Statue of Liberty",  // Your Title here
                        popup: true
                        }
            ]
        });
</script> 
