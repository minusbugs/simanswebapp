<?php include('includes/header.php') ?>
<!-- Page Title
================================================== -->
  <div class="pagetitle">
    <div class="pagetitle-overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <h1>Our Service</h1>
          </div>
          <!-- end .col-md-6 col-sm-6 -->
          
          <div class="col-md-6 col-sm-6">
            <ul class="breadcrumb">
              <li><a href="index.php">Home</a></li>
              <li class="active">Service</li>
            </ul>
          </div>
          <!-- end .col-md-6 col-sm-6 --> 
          
        </div>
        <!-- end .row --> 
      </div>
      <!-- end .container --> 
    </div>
    <!-- end .pagetitle-overlay --> 
  </div>
  <!-- end .pagetitle -->
  <div class="mb-100"> </div>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6">
        <div class="owl-single owl-carousel owl-theme">
          <div class="item">
            <img src="images/features/feature1-1.jpg" alt="" />
          </div>
          
        </div>
        <!-- end .owl-single --> 
      </div>
      <!-- end .col-md-6 col-sm-6 -->
      
      <div class="col-md-6 col-sm-6">
        <h3>Our Service</h3>
        <div class="titleline2"> </div>
        <!-- end .titleline2 -->
        <p>We are proud to claim that we have a strong team of qualified , well experienced expert technicians who have proven be the best in this field . Our team includes qualified professionals who are extremely skilled in their area of know-how . Upholding services to the total satisfaction of our customers at an efficiently reasonable price . Our skills are continuously upgraded for the end user's in areas of product upgradation & safety. We do impart our knowledge sharing with our user’s through proper usage & basic maintenance of equipment’s with all norms  in place . We at SIMANS Elevators desire to serve customers by clearly understanding their needs. </p>
        
        <p>As part of our customer satisfaction guarantee, we invest in operations management; ensuring that our customers are kept fully informed of the progress of our work. We can also talk you through current legislation, giving you a full understanding of what you need to have in place to ensure that your company is fully compliant to regulations. Whatever the size of your project, we will ensure that you receive the best service possible.</p>
        
      </div>
      <!-- end .col-md-6 col-sm-6 --> 
      
    </div>
    <!-- end .row --> 

    <div class="row">
        <div class="col-md-12 col-md-12">
        <p>We are proud of our after sales service; maintaining and repairing your equipment, with a 24 hour call out so that we can be with you whenever you need us. Please feel free to get in touch if you require further information regarding our lift services.</p>
        </div>
    </div>
      <!-- end .row --> 

  </div>
  <!-- end .container -->

  </div>
  <?php include('includes/footer.php') ?>